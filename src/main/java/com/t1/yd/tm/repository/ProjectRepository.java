package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.model.Project;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

}
