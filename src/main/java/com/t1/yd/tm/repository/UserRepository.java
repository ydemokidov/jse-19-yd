package com.t1.yd.tm.repository;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User create(final String login, final String password) {
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(password);
        return user;
    }

    @Override
    public User create(final String login, final String password, final String email) {
        final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @Override
    public User findByLogin(final String login) {
        for (final User user : entities) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : entities) {
            if (user.getEmail().equals(email)) return user;
        }
        return null;
    }


    @Override
    public Boolean isLoginExist(final String login) {
        for (final User user : entities) {
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for (final User user : entities) {
            if (user.getEmail() != null && user.getEmail().equals(email)) return true;
        }
        return false;
    }

}
