package com.t1.yd.tm.api.controller;

public interface IProjectController {

    void createProject();

    void clearProjects();

    void showProjects();

    void showById();

    void showByIndex();

    void removeById();

    void removeByIndex();

    void updateById();

    void updateByIndex();

    void startById();

    void startByIndex();

    void completeById();

    void completeByIndex();

    void changeStatusById();

    void changeStatusByIndex();

}
