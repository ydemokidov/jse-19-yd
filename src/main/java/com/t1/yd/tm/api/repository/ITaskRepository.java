package com.t1.yd.tm.api.repository;

import com.t1.yd.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    List<Task> findAllByProjectId(String projectId);

}
