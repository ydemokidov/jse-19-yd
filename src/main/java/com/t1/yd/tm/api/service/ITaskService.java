package com.t1.yd.tm.api.service;

import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    Task create(String name);

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    List<Task> findAllByProjectId(String projectId);

}
