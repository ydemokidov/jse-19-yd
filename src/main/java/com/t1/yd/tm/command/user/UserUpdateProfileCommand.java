package com.t1.yd.tm.command.user;

import com.t1.yd.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String name = "user_update_profile";

    private final String description = "Update user profile";

    @Override
    public void execute() {
        System.out.println("[UPDATE USER PROFILE]");

        final String userId = getAuthService().getUserId();

        System.out.println("[ENTER LAST NAME:]");
        final String lastName = TerminalUtil.nextLine();
        System.out.println("[ENTER FIRST NAME:]");
        final String fstName = TerminalUtil.nextLine();
        System.out.println("[ENTER MID NAME:]");
        final String midName = TerminalUtil.nextLine();

        getUserService().updateUser(userId, fstName, lastName, midName);

        System.out.println("[USER PROFILE UPDATED]");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }
}
