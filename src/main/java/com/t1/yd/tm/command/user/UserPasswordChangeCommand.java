package com.t1.yd.tm.command.user;

import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;
import com.t1.yd.tm.util.TerminalUtil;

public class UserPasswordChangeCommand extends AbstractUserCommand {

    private final String name = "user_password_change";

    private final String description = "Change user password";

    @Override
    public void execute() {
        System.out.println("[CHANGE USER PASSWORD]");
        final User user = getAuthService().getUser();
        System.out.println("ENTER NEW PASSWORD:");
        final String newPwd = TerminalUtil.nextLine();
        user.setPasswordHash(HashUtil.salt(newPwd));
        System.out.println("[PASSWORD CHANGED]");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
