package com.t1.yd.tm.command.system;

public class ApplicationExitCommand extends AbstractSystemCommand {

    public static final String NAME = "exit";
    public static final String ARGUMENT = null;
    public static final String DESCRIPTION = "Exit program";

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }
}
