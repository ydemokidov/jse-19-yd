package com.t1.yd.tm.service;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.IUserService;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.entity.UserNotFoundException;
import com.t1.yd.tm.exception.field.EmailEmptyException;
import com.t1.yd.tm.exception.field.IdEmptyException;
import com.t1.yd.tm.exception.field.LoginEmptyException;
import com.t1.yd.tm.exception.field.PasswordEmptyException;
import com.t1.yd.tm.exception.user.IsEmailExistException;
import com.t1.yd.tm.exception.user.IsLoginExistException;
import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.HashUtil;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    public UserService(IUserRepository repository) {
        super(repository);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        repository.removeById(user.getId());
        return user;
    }

    @Override
    public User removeByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        final User user = findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        repository.removeById(user.getId());
        return user;
    }

    @Override
    public User setPassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(password));
        return user;
    }

    @Override
    public User updateUser(final String id, final String firstName, final String lastName, final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        final User user = repository.findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        return user;
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        return repository.create(login, HashUtil.salt(password));
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isEmailExist(email)) throw new IsEmailExistException();
        return repository.create(login, HashUtil.salt(password), email);
    }

    @Override
    public User create(final String login, final String password, Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new IsLoginExistException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) role = Role.USUAL;
        return repository.create(login, HashUtil.salt(password), role);
    }


    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    public User findByEmail(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }


    @Override
    public Boolean isLoginExist(final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.isLoginExist(login);
    }

    @Override
    public Boolean isEmailExist(final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.isEmailExist(email);
    }

}
